package lesson09.task01.figure.triangle;

import lesson09.task01.figure.Figure;

public class Triangle extends Figure {


    private Point pointA = new Point();
    private Point pointB = new Point();
    private Point pointC = new Point();

    private double sizeAB;
    private double sizeBC;
    private double sizeAC;

    public Triangle(int ax, int ay, int bx, int by, int cx, int cy) {
        pointA.setX(ax);
        pointA.setY(ay);
        pointB.setX(bx);
        pointB.setY(by);
        pointC.setX(cx);
        pointC.setY(cy);
        setSizeAB();
        setSizeBC();
        setSizeAC();
    }

    public double calculatePerimeter() {
        return sizeAB + sizeBC + sizeAC;
    }

    public double calculateSquare() {
        return Math.abs((pointB.getX() - pointA.getX()) * (pointC.getY() - pointA.getY()) - (pointC.getX() - pointA.getX()) * (pointB.getY() - pointA.getY())) / 2;
    }

    // расчитываем длину стороны треугольника по координатам его вершин
    public void setSizeAB() {
        double sizeAB = Math.sqrt(Math.pow(pointB.getX() - pointA.getX(), 2) + Math.pow(pointB.getY() - pointA.getY(), 2));
        this.sizeAB = Math.abs(sizeAB);
    }

    public void setSizeBC() {
        double sizeBC = Math.sqrt(Math.pow(pointC.getX() - pointB.getX(), 2) + Math.pow(pointC.getY() - pointB.getY(), 2));
        this.sizeBC = Math.abs(sizeBC);
    }

    public void setSizeAC() {
        double sizeAC = Math.sqrt(Math.pow(pointC.getX() - pointA.getX(), 2) + Math.pow(pointC.getY() - pointA.getY(), 2));
        this.sizeAC = Math.abs(sizeAC);
    }
}

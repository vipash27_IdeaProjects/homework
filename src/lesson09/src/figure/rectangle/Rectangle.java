package lesson09.task01.figure.rectangle;

import lesson09.task01.figure.Figure;

public class Rectangle extends Figure {

    private double length;
    private double width;

    public Rectangle(double length, double width) {
        this.length = length;
        this.width = width;
    }

    public double calculatePerimeter() {
        return (length + width) * 2;
    }

    public double calculateSquare() {
        return length * width;
    }
}

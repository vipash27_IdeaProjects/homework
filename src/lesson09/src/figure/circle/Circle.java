package lesson09.src.figure.circle;

import lesson09.task01.figure.Figure;

public class Circle extends Figure {

    private double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    public double calculatePerimeter() {
        return 2 * Math.PI * radius;
    }

    public double calculateSquare() {
        return Math.PI * Math.pow(radius, 2);
    }
}

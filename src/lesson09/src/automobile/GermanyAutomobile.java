package lesson09.src.automobile;

import java.util.Random;

public class GermanyAutomobile extends Automobile {

    private Random random = new Random();

    int getRate() {
        int minRate = 7;
        int maxRate = 10;
        return random.nextInt(maxRate - minRate) + minRate;
    }

    public String getDescription() {
        return super.getDescription() + "made in Germany. Rate: " + getRate();
    }
}

package lesson09.src.automobile;

public abstract class Automobile {

    abstract int getRate();

    public String getDescription() {
        return "Automobile: ";
    }
}

package lesson09.src.automobile;

import java.util.Random;

public class EnglandAutomobile extends Automobile {

    private Random random = new Random();

    int getRate() {
        int minRate = 6;
        int maxRate = 9;
        return random.nextInt(maxRate - minRate) + minRate;
    }

    public String getDescription() {
        return super.getDescription() + "made in England. Rate: " + getRate();
    }
}

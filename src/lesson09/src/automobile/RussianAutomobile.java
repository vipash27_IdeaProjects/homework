package lesson09.src.automobile;

import java.util.Random;

public class RussianAutomobile extends Automobile {

    private Random random = new Random();

    int getRate() {
        return random.nextInt(6);
    }

     public String getDescription() {
        return super.getDescription() + "made in Russia. Rate: " + getRate();
    }
}

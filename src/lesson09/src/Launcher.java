package lesson09.src;

import lesson09.src.employe.Employe;
//import lesson09.task01.figure.circle.Circle;
import lesson09.task01.figure.Figure;
import lesson09.task01.figure.rectangle.Rectangle;
import lesson09.task01.figure.triangle.Triangle;
import lesson09.src.automobile.Automobile;
import lesson09.src.automobile.EnglandAutomobile;
import lesson09.src.automobile.GermanyAutomobile;
import lesson09.src.automobile.RussianAutomobile;
import lesson09.src.employe.Accountant;
import lesson09.src.employe.Director;
import lesson09.src.employe.Worker;

/*
1. Написать иерархию классов «Фигуры». Фигура -> Треугольник -> Прямоугольник -> Круг. Реализовать ф-ю подсчета площади для
каждого типа фигуры и подсчет периметра. Создать массив из 5 фигур. Вывести на экран сумму периметра всех фигур в массиве.
2. Создать класс Automobile. Сделать его абстрактным. Добавить в класс абстрактный метод  int getRate(). Добавить в класс метод
String getDescription(), который возвращает строку "Automobile: ". Создать класс RussianAutomobile, который наследуется от
Automobile. Создать класс GermanyAutomobile, который наследуется от Automobile. Cоздать класс EnglandAutomobile, который
наследуется от Automobile. В каждом из классов написать свою реализацию метода getRate. Метод должен возвращать рейтинг
автомобиля в зависимости от типа. В каждом из классов написать свою реализацию метода getDescription. Методы должны возвращать
строку вида: <getDescription() родительского класса>  + <" Моя страна - Sssss. Рейтинг автомобиля N">, где Sssss - название
страны, где N - рейтинг автомобиля
3. Разработать программу для формирования платежной ведомости на предприятии.
	a)Предусмотреть 4 типа сотрудников в зависимости от варианта расчета заработной платы:
		месячная ставка
 		почасовая оплата
		% от объема продаж
		базовая месячная ставка + % от объема продаж
	b)Для каждого сотрудника хранить имя, фамилию, информацию о должности (наименование,отдел)
4. Создать классы "Директор", "Рабочий", "Бухгалтер". Реализовать интерфейс с методом, который печатает название должности и
имплементировать этот метод в созданные классы.
*/
public class Launcher {

    public static void main(String[] args) {
        // task01
        Figure[] figures = new Figure[5];
        figures[0] = new Triangle(1, 6, 4, 2, 3, 7);
        figures[1] = new Rectangle(4, 5);
        figures[2] = new Rectangle(7, 9);
        //figures[3] = new Circle(5);
        //figures[4] = new Circle(3);
        double result = calculateSumPerimeters(figures);
        System.out.println("Сумма периметра всех фигур в массиве: " + result);
        System.out.println("-------------------------------------------------");
        // task02
        Automobile automobile1 = new RussianAutomobile();
        Automobile automobile2 = new EnglandAutomobile();
        Automobile automobile3 = new GermanyAutomobile();
        System.out.println(automobile1.getDescription());
        System.out.println(automobile2.getDescription());
        System.out.println(automobile3.getDescription());
        System.out.println("-------------------------------------------------");
        // task03
        Employe employe = new Accountant("Иван", "Иванов", "Финансовый отдел", "Главный бухгалтер");
        System.out.println(employe.getTotalPay());
        System.out.println("-------------------------------------------------");
        // task04
        EmployeService employe1 = new Director();
        EmployeService employe2 = new Worker();
        EmployeService employe3 = new Accountant();

        employe1.printInfo();
        employe2.printInfo();
        employe3.printInfo();

    }

    private static double calculateSumPerimeters(Figure[] figures) {
        double result = 0;
        for (int i = 0; i < figures.length; i++) {
            result += figures[i].calculatePerimeter();
        }
        return result;
    }
}

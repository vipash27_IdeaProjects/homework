package lesson09.src.employe;

public class Worker extends Employe {

    public Worker() {

    }

    public Worker(String firstName, String lastName, String departmentName, String position) {
        super(firstName, lastName, departmentName, position);
    }

    public void printInfo() {
        System.out.println("Worker");
    }
}

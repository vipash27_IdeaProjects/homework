package lesson09.src.employe;

public class Director extends Employe {

    public Director() {

    }

    public Director(String firstName, String lastName, String departmentName, String position) {
        super(firstName, lastName, departmentName, position);
    }

    public void printInfo() {
        System.out.println("Director");
    }

}

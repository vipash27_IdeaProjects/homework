package lesson09.src.employe;

public class Accountant extends Employe {

    public Accountant() {

    }

    public Accountant(String firstName, String lastName, String departmentName, String position) {
        super(firstName, lastName, departmentName, position);

    }

    public void printInfo() {
        System.out.println("Accountant");
    }
}

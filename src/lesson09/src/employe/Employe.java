package lesson09.src.employe;

import lesson09.src.PaymentStatement;
import lesson09.src.EmployeService;

import java.math.BigDecimal;

public class Employe implements EmployeService, PaymentStatement {

    private String firstName;
    private String lastName;
    private String departmentName;
    private String position;

    public Employe() {

    }

    public Employe(String firstName, String lastName, String departmentName, String position) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.departmentName = departmentName;
        this.position = position;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public void printInfo() {

    }

    public BigDecimal getTotalPay() {
        return null;
    }

    public BigDecimal calculateMonthlyRate() {
        return null;
    }

    public BigDecimal calculateHourlyPayment() {
        return null;
    }

    public double calculatePercentageOfSales() {
        return 0;
    }

    public double calculateBaseMonthlyRate() {
        return 0;
    }
}

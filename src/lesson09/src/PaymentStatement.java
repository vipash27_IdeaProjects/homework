package lesson09.src;

import java.math.BigDecimal;

public interface PaymentStatement {

    BigDecimal getTotalPay();

    BigDecimal calculateMonthlyRate();

    BigDecimal calculateHourlyPayment();

    double calculatePercentageOfSales();

    double calculateBaseMonthlyRate();
}

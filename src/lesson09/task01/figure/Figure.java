package lesson09.task01.figure;

public abstract class Figure {

    public double calculatePerimeter() {
        return 0;
    }

    public double calculateSquare() {
        return 0;
    }
}

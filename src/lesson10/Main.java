package lesson10;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<String> arrayList = new ArrayList<>();
        arrayList.add("test1");
        arrayList.add("test2");
        for (String str : arrayList) {
            System.out.println(str);
        }
        arrayList.add(0, "test3");
        arrayList.set(0, "test4");
        System.out.println("-------------------------");
        for (int i = 0; i < arrayList.size(); i++) {
            System.out.println(arrayList.get(i));
        }


        List linkedList = new LinkedList<>();
    }
}

class Student {
    private String name;
    private int age;
    private int course;

    public Student(int age, int course) {
        this.age = age;
        this.course = course;
        this.name = "Name" + age + " " + course;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public int getCourse() {
        return course;
    }

    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", course=" + course +
                '}';
    }
}